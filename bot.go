package main

import (
	"circle_triangle/configs"
	"circle_triangle/internal/effects"
	"circle_triangle/internal/events"
	"circle_triangle/internal/keyset"
	"circle_triangle/internal/messages"
	"circle_triangle/internal/processors"
	"context"
	"flag"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/url"
	"strconv"
)

func main() {
	conf := getAppConf()
	conn, err := buildSocketConn(conf)
	ctx := buildCtx(conf, conn)
	defer func(conn *websocket.Conn) {
		err := conn.Close()
		logError(err)
	}(conn)
	logError(err)
	rawCh := make(chan []byte, 10)
	pipeProcessors(ctx, rawCh)
	for {
		_, raw, err := conn.ReadMessage()
		logError(err)
		rawCh <- raw
	}
}

func buildCtx(conf *configs.Config, conn *websocket.Conn) context.Context {
	ctx := context.Background()
	ctx = context.WithValue(ctx, keyset.SocketConnection, conn)
	ctx = context.WithValue(ctx, keyset.AppConfig, conf)
	ctx = context.WithValue(ctx, keyset.RuntimeConfig, effects.DefaultMode)
	return ctx
}

func pipeProcessors(ctx context.Context, rawCh chan []byte) {
	eventCh := make(chan []byte)                            // cq-http 事件
	msgCh := make(chan []byte)                              // cq-http 消息
	noticeCh := make(chan []byte)                           // cq-http 通知
	heartbeatCh := make(chan *messages.RawHeartbeatMsg)     // cq-http 心跳
	rawQQMsg := make(chan *messages.RawQQMsg)               // cq-http qq消息
	selectedQQMsg := make(chan *messages.RawQQMsg)          // 按规则过滤特定的qq群
	repeatWnCh := make(chan *events.RepeatWarning)          // 复读检测
	pokeNoticeCh := make(chan *messages.RawPokeNotice)      // 戳一戳检测
	orderActivationCh := make(chan *messages.RawPokeNotice) // 指令激活
	orderMsgCh := make(chan *messages.RawQQMsg)             // 指令消息
	orderCh := make(chan *effects.Order)                    // 指令
	processors.RootShuntProcessor(ctx, rawCh)(eventCh, msgCh, noticeCh)
	processors.MetaEventShuntProcessor(ctx)(eventCh, heartbeatCh)
	processors.MessageShuntProcessor(ctx)(msgCh, rawQQMsg, orderMsgCh)
	processors.NoticeShuntProcessor(ctx)(noticeCh, pokeNoticeCh)
	processors.TopOrderSelectProcessor(ctx)(orderMsgCh, orderCh)
	processors.OrderActivationProcessor(ctx)(pokeNoticeCh, orderActivationCh)
	processors.TopOrderProcessor(ctx)(orderActivationCh, orderCh)
	processors.LogHeartbeatProcessor(ctx)(heartbeatCh)
	processors.QQGroupSelectProcessor(ctx)(rawQQMsg, selectedQQMsg)
	processors.RepeatDetectionProcessor(ctx)(selectedQQMsg, repeatWnCh)
	processors.MuteProcessor(ctx)(repeatWnCh)
}

func getAppConf() *configs.Config {
	env := flag.String("env", "dev", "runtime env")
	flag.Parse()
	conf := configs.Load(*env)
	fmt.Println(conf.AppName)
	return conf
}

func buildSocketConn(conf *configs.Config) (*websocket.Conn, error) {
	u := url.URL{
		Host:   conf.CqHttp.SocketHost + ":" + strconv.Itoa(int(conf.CqHttp.SocketPort)),
		Scheme: "ws",
		Path:   "/",
	}
	log.Printf("connecting to %s", u.String())
	conn, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	return conn, err
}

func logError(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
