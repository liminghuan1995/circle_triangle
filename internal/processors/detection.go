package processors

import (
	"circle_triangle/internal/events"
	"circle_triangle/internal/messages"
	"context"
)

type tuples2 struct {
	prev    string
	current string
}

func (t *tuples2) isSame() bool {
	return t.prev == t.current
}

var t *tuples2

func RepeatDetectionProcessor(ctx context.Context) func(
	in chan *messages.RawQQMsg,
	out chan *events.RepeatWarning,
) {
	t = new(tuples2)
	return func(in chan *messages.RawQQMsg, out chan *events.RepeatWarning) {
		go func() {
			for msg := range in {
				t.current = msg.RawMessage
				if t.isSame() {
					warning := new(events.RepeatWarning)
					warning.Content = msg.Message
					warning.GroupId = msg.GroupId
					warning.UserId = msg.UserId
					warning.Time = msg.Time
					out <- warning
				}
				t.prev = msg.RawMessage
				t.current = ""
			}
			close(out)
		}()
	}
}
