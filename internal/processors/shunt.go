package processors

import (
	"bytes"
	"circle_triangle/internal/effects"
	"circle_triangle/internal/messages"
	"context"
	"encoding/json"
	"log"
)

func printDecodingError(err error) {
	log.Println("Error when decoding JSON.")
	log.Println(err)
}

func printUnhandledMsgType(binary []byte) {
	log.Println(string(binary))
}

func decodeJSON(x interface{}, binary []byte) error {
	r := bytes.NewReader(binary)
	decoder := json.NewDecoder(r)
	err := decoder.Decode(&x)
	return err
}

func RootShuntProcessor(ctx context.Context, rawCh chan []byte) func(
	metaEvCh chan<- []byte,
	msgCh chan<- []byte,
	noticeCh chan<- []byte,
) {
	return func(
		metaEvCh chan<- []byte,
		msgCh chan<- []byte,
		noticeCh chan<- []byte,
	) {
		go func() {
			for raw := range rawCh {
				msg := new(messages.RawBaseMsg)
				err := decodeJSON(msg, raw)
				if err != nil {
					printDecodingError(err)
					continue
				}
				switch msg.PostType {
				case "meta_event":
					metaEvCh <- raw
				case "message":
					msgCh <- raw
				case "notice":
					noticeCh <- raw
				default:
					printUnhandledMsgType(raw)
				}
			}
			close(metaEvCh)
			close(msgCh)
			close(noticeCh)
		}()
	}
}

func MetaEventShuntProcessor(ctx context.Context) func(
	<-chan []byte,
	chan<- *messages.RawHeartbeatMsg,
) {
	return func(evCh <-chan []byte, hbCh chan<- *messages.RawHeartbeatMsg) {
		go func() {
			hasBroadcast := false
			for raw := range evCh {
				ev := new(messages.MetaEventMsg)
				err := decodeJSON(ev, raw)
				if err != nil {
					printDecodingError(err)
					continue
				}
				switch ev.MetaEventType {
				case "heartbeat":
					hb := new(messages.RawHeartbeatMsg)
					err := decodeJSON(hb, raw)
					if err != nil {
						printDecodingError(err)
						continue
					}
					hbCh <- hb
					if !hasBroadcast {
						err = effects.Greeting(ctx)
						if err != nil {
							log.Println(err)
						} else {
							hasBroadcast = true
						}
					}
				default:
					printUnhandledMsgType(raw)
				}
			}
			close(hbCh)
		}()
	}
}

func MessageShuntProcessor(ctx context.Context) func(
	<-chan []byte,
	chan<- *messages.RawQQMsg,
	chan<- *messages.RawQQMsg,
) {
	return func(
		msgCh <-chan []byte,
		qqMsgCh chan<- *messages.RawQQMsg,
		orderMsgCh chan<- *messages.RawQQMsg,
	) {
		go func() {
			for raw := range msgCh {
				qqMsg := new(messages.RawQQMsg)
				err := decodeJSON(qqMsg, raw)
				if err != nil {
					printDecodingError(err)
					continue
				}
				qqMsgCh <- qqMsg
				orderMsgCh <- qqMsg
			}
			close(qqMsgCh)
		}()
	}
}

func NoticeShuntProcessor(ctx context.Context) func(
	<-chan []byte,
	chan<- *messages.RawPokeNotice,
) {
	return func(
		noticeCh <-chan []byte,
		pokeCh chan<- *messages.RawPokeNotice,
	) {
		go func() {
			for raw := range noticeCh {
				notice := new(messages.RawNoticeMsg)
				err := decodeJSON(notice, raw)
				if err != nil {
					printDecodingError(err)
					continue
				}
				switch notice.SubType {
				case "poke":
					poke := new(messages.RawPokeNotice)
					err = decodeJSON(poke, raw)
					if err != nil {
						printDecodingError(err)
						continue
					}
					pokeCh <- poke
				default:
					printUnhandledMsgType(raw)
				}
			}
			close(pokeCh)
		}()
	}
}
