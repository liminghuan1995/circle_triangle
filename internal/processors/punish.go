package processors

import (
	"circle_triangle/internal/effects"
	"circle_triangle/internal/events"
	"circle_triangle/internal/keyset"
	"circle_triangle/internal/response"
	"context"
	"github.com/gorilla/websocket"
	"log"
	"strconv"
)

type muteParams struct {
	GroupId  int `json:"group_id"`
	UserId   int `json:"user_id"`
	Duration int `json:"duration"`
}
type bdParams struct {
	GroupId int    `json:"group_id"`
	Message string `json:"message"`
}

func sendMute(conn *websocket.Conn, w *events.RepeatWarning) error {
	res := new(response.SocketResponse)
	res.Action = response.ActionMuteSomebodyInGroup
	res.Params = muteParams{
		GroupId:  w.GroupId,
		UserId:   w.UserId,
		Duration: 10,
	}
	err := conn.WriteJSON(res)
	return err
}

func sendSolution(conn *websocket.Conn, w *events.RepeatWarning) error {
	res := new(response.SocketResponse)
	msg := w.GetMessage()
	res.Action = response.ActionSendGroupMsg
	res.Params = bdParams{
		GroupId: w.GroupId,
		Message: msg,
	}
	err := conn.WriteJSON(res)
	return err
}

func sendAt(conn *websocket.Conn, w *events.RepeatWarning) error {
	res := new(response.SocketResponse)
	at := new(response.CQCode)
	at.Action = response.CqCodeAt
	at.Fields = map[string]string{"qq": strconv.Itoa(w.UserId)}
	content := at.FormatJson()
	res.Params = bdParams{
		GroupId: w.GroupId,
		Message: content,
	}
	err := conn.WriteJSON(res)
	return err
}

func sendWarning(conn *websocket.Conn, w *events.RepeatWarning) error {
	res := new(response.SocketResponse)
	res.Action = response.ActionSendGroupMsg
	res.Params = bdParams{
		GroupId: w.GroupId,
		Message: "🎾⚠️",
	}
	err := conn.WriteJSON(res)
	return err
}

func MuteProcessor(ctx context.Context) func(warnings <-chan *events.RepeatWarning) {
	var conn *websocket.Conn
	var rt *effects.RuntimeConfig
	conn = ctx.Value(keyset.SocketConnection).(*websocket.Conn)
	rt = ctx.Value(keyset.RuntimeConfig).(*effects.RuntimeConfig)
	return func(warnings <-chan *events.RepeatWarning) {
		go func() {
			for w := range warnings {
				var err error
				switch rt.Mode {
				case effects.DestructionMode:
					err = sendMute(conn, w)
					if err != nil {
						log.Print(err)
					}
					err = sendSolution(conn, w)
					if err != nil {
						log.Print(err)
					}
				case effects.MonitoringMode:
					//err = sendAt(conn, w)
					//if err != nil {
					//	log.Print(err)
					//}
					err = sendWarning(conn, w)
					if err != nil {
						log.Print(err)
					}
				}
			}
		}()
	}
}
