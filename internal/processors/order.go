package processors

import (
	"circle_triangle/internal/effects"
	"circle_triangle/internal/messages"
	"context"
	"fmt"
	"log"
)

func TopOrderProcessor(ctx context.Context) func(<-chan *messages.RawPokeNotice, <-chan *effects.Order) {
	return func(pokeCh <-chan *messages.RawPokeNotice, odCh <-chan *effects.Order) {
		actCh := make(chan struct{})
		cmdCh := make(chan *effects.Order)
		go func() {
			go func() {
				for range pokeCh {
					select {
					case actCh <- struct{}{}:
						go func() {
							err := effects.SendDirectiveActivated(ctx, true)
							if err != nil {
								log.Println(err)
							}
						}()
					default:
						log.Println("忽略指令激活")
					}
				}
			}()
			go func() {
				for od := range odCh {
					select {
					case cmdCh <- od:
						od.Execute(ctx)
					default:
						log.Println("忽略指令")
					}
				}
			}()
			for {
				<-actCh
				cmd := <-cmdCh
				fmt.Println(cmd.Command)
			}
		}()
	}
}
