package processors

import (
	"circle_triangle/configs"
	"circle_triangle/internal/effects"
	"circle_triangle/internal/keyset"
	"circle_triangle/internal/messages"
	"context"
)

func QQGroupSelectProcessor(ctx context.Context) func(
	in <-chan *messages.RawQQMsg,
	out chan<- *messages.RawQQMsg,
) {
	var conf *configs.Config
	conf = ctx.Value(keyset.AppConfig).(*configs.Config)
	groupId := conf.QQ.GroupId
	return func(in <-chan *messages.RawQQMsg, out chan<- *messages.RawQQMsg) {
		go func() {
			for msg := range in {
				if msg.GroupId == groupId {
					out <- msg
				}
			}
			close(out)
		}()
	}
}

func TopOrderSelectProcessor(ctx context.Context) func(<-chan *messages.RawQQMsg, chan<- *effects.Order) {
	ordersMap := map[string]struct{}{
		effects.SwitchToMonitoringMode:  {},
		effects.SwitchToDestructionMode: {},
		effects.Clean:                   {},
	}
	conf := ctx.Value(keyset.AppConfig).(*configs.Config)
	commanderId := conf.QQ.MaintainerId
	return func(msgCh <-chan *messages.RawQQMsg, odCh chan<- *effects.Order) {
		go func() {
			for msg := range msgCh {
				content := msg.RawMessage
				od := new(effects.Order)
				_, ok := ordersMap[content]
				if ok && msg.UserId == commanderId {
					od.Command = content
					odCh <- od
				}
			}
		}()
	}
}

func OrderActivationProcessor(ctx context.Context) func(
	<-chan *messages.RawPokeNotice,
	chan<- *messages.RawPokeNotice,
) {
	conf := ctx.Value(keyset.AppConfig).(*configs.Config)
	commanderId := conf.QQ.MaintainerId
	botSelfId := conf.QQ.RobotId
	return func(pokeCh <-chan *messages.RawPokeNotice, detectedCh chan<- *messages.RawPokeNotice) {
		go func() {
			for poke := range pokeCh {
				activate := poke.SenderId == commanderId && poke.TargetId == botSelfId
				if activate {
					detectedCh <- poke
				}
			}
		}()
	}
}
