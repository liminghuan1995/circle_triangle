package processors

import (
	"circle_triangle/internal/messages"
	"context"
	"fmt"
	"time"
)

func LogHeartbeatProcessor(ctx context.Context) func(hbCh <-chan *messages.RawHeartbeatMsg) {
	return func(hbCh <-chan *messages.RawHeartbeatMsg) {
		go func() {
			for hb := range hbCh {
				t := time.Unix(int64(hb.Time), 0)
				date := t.Format("2006-01-02 03:04:05")
				fmt.Printf("[%s] cq-http heartbeat\n", date)
			}
		}()
	}
}
