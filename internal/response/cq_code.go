package response

import "fmt"

const (
	CqCodeAt   = "at"
	CqCodePoke = "poke"
)

type CQCode struct {
	Action string
	Fields map[string]string
}

func (c *CQCode) FormatJson() string {
	// todo: build json-formatted cq-code
	msg := "[CQ:"
	msg += c.Action
	for k, v := range c.Fields {
		kv := fmt.Sprintf(",%s=%s", k, v)
		msg += kv
	}
	msg += "]"
	return msg
}
