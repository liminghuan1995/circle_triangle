package response

type SocketResponse struct {
	Action string      `json:"action"`
	Params interface{} `json:"params"`
}

const (
	ActionSendGroupMsg        = "send_group_msg"
	ActionDeleteMsg           = "delete_msg"
	ActionMuteSomebodyInGroup = "set_group_ban"
)
