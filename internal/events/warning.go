package events

import "fmt"

type RepeatWarning struct {
	UserId  int
	GroupId int
	Content string
	Time    int
}

func (r *RepeatWarning) GetMessage() string {
	var msg string
	title := "检测到违规行为：【复读】\n"
	content := fmt.Sprintf("复读内容：【%s...】\n", r.Content)
	action := "执行操作：【禁言】\n"
	msg = title + content + action
	return msg
}
