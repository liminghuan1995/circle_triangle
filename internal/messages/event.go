package messages

type RawBaseMsg struct {
	SelfId   int    `json:"self_id"`
	Time     int    `json:"time"`
	PostType string `json:"post_type"`
}
