package messages

type MetaEventMsg struct {
	RawBaseMsg
	MetaEventType string `json:"meta_event_type"`
}

type RawHeartbeatMsg struct {
	MetaEventMsg
	Interval int `json:"interval"`
	Status   struct {
	} `json:"status"`
}
