package messages

type RawQQMsg struct {
	RawBaseMsg
	SubType     string `json:"sub_type"`
	Message     string `json:"message"`
	MessageId   int    `json:"message_id"`
	MessageSeq  int    `json:"message_seq"`
	MessageType string `json:"message_type"`
	UserId      int    `json:"user_id"`
	RawMessage  string `json:"raw_message"`
	GroupId     int    `json:"group_id,omitempty"`
}

type PrivateQQMsg struct {
	RawQQMsg
	SelfId     int `json:"self_id"`
	TempSource int `json:"temp_source"`
}

type GroupQQMsg struct {
	RawQQMsg
	GroupId int `json:"group_id,omitempty"`
	Sender  struct {
		Age      int    `json:"age"`
		Area     string `json:"area"`
		Card     string `json:"card"`
		Level    string `json:"level"`
		Nickname string `json:"nickname"`
		Role     string `json:"role"`
		Title    string `json:"title"`
		UserId   int    `json:"user_id"`
	} `json:"sender"`
}
