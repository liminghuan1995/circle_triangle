package messages

type RawNoticeMsg struct {
	RawBaseMsg
	SubType  string `json:"sub_type"`
	TargetId int    `json:"target_id"`
}

type RawPokeNotice struct {
	RawNoticeMsg
	GroupId  int `json:"group_id"`
	SenderId int `json:"sender_id"`
	TargetId int `json:"target_id"`
	Time     int `json:"time"`
	UserId   int `json:"user_id"`
}
