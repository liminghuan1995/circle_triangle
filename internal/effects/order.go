package effects

import (
	"circle_triangle/internal/keyset"
	"context"
	"log"
	"time"
)

const (
	SwitchToDestructionMode = "启用歼灭模式"
	SwitchToMonitoringMode  = "启用监督模式"
	Clean                   = "清扫"
)

type Order struct {
	Command   string
	Lifecycle struct {
		Deadline time.Duration
		timer    time.Timer
	}
}

func (od *Order) Execute(ctx context.Context) {
	switch od.Command {
	case SwitchToDestructionMode:
		doSwitchToAnyMode(ctx, DestructionMode)
	case SwitchToMonitoringMode:
		doSwitchToAnyMode(ctx, MonitoringMode)
	case Clean:
		doClean(ctx)
	}
}

func doSwitchToAnyMode(ctx context.Context, m Mode) {
	var rt *RuntimeConfig
	rt = ctx.Value(keyset.RuntimeConfig).(*RuntimeConfig)
	rt.SwitchMode(m)
	err := SendModeSwitched(ctx, rt.GetModeName())
	if err != nil {
		log.Println(err)
	}
}

func doClean(ctx context.Context) {}
