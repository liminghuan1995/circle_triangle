package effects

import (
	"circle_triangle/configs"
	"circle_triangle/internal/keyset"
	"circle_triangle/internal/response"
	"context"
	"fmt"
	"github.com/gorilla/websocket"
)

func GetConn(ctx context.Context) *websocket.Conn {
	return ctx.Value(keyset.SocketConnection).(*websocket.Conn)
}

func GetConf(ctx context.Context) *configs.Config {
	return ctx.Value(keyset.AppConfig).(*configs.Config)
}

func GetRuntime(ctx context.Context) *RuntimeConfig {
	return ctx.Value(keyset.RuntimeConfig).(*RuntimeConfig)
}

func Greeting(ctx context.Context) error {
	conn := GetConn(ctx)
	conf := GetConf(ctx)
	rt := GetRuntime(ctx)
	msg := new(response.SocketResponse)
	msg.Action = response.ActionSendGroupMsg
	type params struct {
		GroupId int    `json:"group_id"`
		Message string `json:"message"`
	}
	title := "系统已启动\n"
	name := fmt.Sprintf("系统名称：【%s】\n", conf.AppName)
	version := "运行版本：【原型机】\n"
	mode := fmt.Sprintf("系统模式：【%s】", rt.GetModeName())
	content := title + name + version + mode
	msg.Params = params{
		GroupId: conf.QQ.GroupId,
		Message: content,
	}
	err := conn.WriteJSON(*msg)
	return err
}

func SendDirectiveActivated(ctx context.Context, isTopLevel bool) error {
	conn := GetConn(ctx)
	conf := GetConf(ctx)
	msg := new(response.SocketResponse)
	msg.Action = response.ActionSendGroupMsg
	type params struct {
		GroupId int    `json:"group_id"`
		Message string `json:"message"`
	}
	var content string
	if isTopLevel {
		content = "【接受最高级指令】"
	} else {
		content = "【接受指令】"
	}
	msg.Params = params{
		GroupId: conf.QQ.GroupId,
		Message: content,
	}
	err := conn.WriteJSON(*msg)
	return err
}

func SendModeSwitched(ctx context.Context, modeName string) error {
	conn := GetConn(ctx)
	conf := GetConf(ctx)
	msg := new(response.SocketResponse)
	msg.Action = response.ActionSendGroupMsg
	type params struct {
		GroupId int    `json:"group_id"`
		Message string `json:"message"`
	}
	content := fmt.Sprintf("【运行模式变更：%s】", modeName)
	msg.Params = params{
		GroupId: conf.QQ.GroupId,
		Message: content,
	}
	err := conn.WriteJSON(*msg)
	return err
}
