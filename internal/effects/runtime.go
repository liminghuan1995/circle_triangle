package effects

import "time"

type Mode int

const (
	DestructionMode = iota
	MonitoringMode
)

type RuntimeConfig struct {
	MuteDuration time.Duration // 禁言时常
	Mode         Mode          // 运行模式
}

var DefaultMode *RuntimeConfig

var ModeNames map[Mode]string

func init() {
	ModeNames = map[Mode]string{
		DestructionMode: "歼灭模式",
		MonitoringMode:  "监督模式",
	}
	DefaultMode = &RuntimeConfig{
		MuteDuration: time.Second * 60 * 30,
		Mode:         MonitoringMode,
	}
}

func (c *RuntimeConfig) GetModeName() string {
	return ModeNames[c.Mode]
}

func (c *RuntimeConfig) SwitchMode(m Mode) {
	c.Mode = m
}
