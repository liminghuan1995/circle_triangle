module circle_triangle

go 1.16

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20220512140231-539c8e751b99
)
