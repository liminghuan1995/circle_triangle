package configs

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
)

type Config struct {
	AppName string `yaml:"app_name"`
	QQ      struct {
		GroupId      int `yaml:"group_id"`
		RobotId      int `yaml:"robot_id"`
		MaintainerId int `yaml:"maintainer_id"`
	} `yaml:"qq"`
	CqHttp struct {
		SocketHost string `yaml:"socket_host"`
		SocketPort uint64 `yaml:"socket_port"`
	} `yaml:"cq_http"`
}

func Load(env string) *Config {
	filename := fmt.Sprintf("%s.config.yaml", env)
	fp := path.Join("./configs", filename)
	fp, err := filepath.Abs(fp)
	logErr(err)
	f, err := os.Open(fp)
	defer func(f *os.File) {
		err := f.Close()
		logErr(err)
	}(f)
	logErr(err)
	var conf Config
	buf, err := ioutil.ReadFile(fp)
	logErr(err)
	err = yaml.Unmarshal(buf, &conf)
	logErr(err)
	return &conf
}

func logErr(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
